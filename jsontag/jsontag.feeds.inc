<?php
/**
 * @file
 * Feeds mapping implementation for the Jsontag module.
 */

/**
 * Implements hook_feeds_processor_targets_alter().
 */
function jsontag_feeds_processor_targets_alter(&$targets, $entity_type, $bundle) {
  if (jsontag_entity_supports_jsontags($entity_type)) {
    $info = jsontag_get_info();
    foreach ($info['tags'] as $name => $tag) {
      $targets['json_' . $name] = array(
        'name' => 'Json tag: ' . check_plain($tag['label']),
        'callback' => 'jsontag_feeds_set_target',
        'description' => $tag['description'],
      );
    }
  }
}

/**
 * Callback function to set value of a jsontag tag.
 */
function jsontag_feeds_set_target($source, $entity, $target, $value) {
  // Don't do anything if we weren't given any data.
  if (empty($value)) {
    return;
  }

  // Strip the prefix that was added above.
  $name = str_replace('json_', '', $target);

  // Assign the value.
  $entity->jsontags[$name]['value'] = $value;
}
