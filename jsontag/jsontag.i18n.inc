<?php
/**
 * @file
 * Internationalization (i18n) hooks.
 */

/**
 * Implements hook_i18n_string_info().
 */
function jsontag_i18n_string_info() {
  $groups['jsontag'] = array(
    'title' => t('Jsontag'),
    'description' => t('Configurable jsontags.'),
    'format' => FALSE,
    'list' => FALSE,
  );
  return $groups;
}
