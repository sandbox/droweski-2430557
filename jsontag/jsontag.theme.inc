<?php

/**
 * @file
 * Theme callbacks for the jsontag module.
 */

/**
 * Theme callback for a normal json tag.
 *
 */
function theme_jsontag($variables) {
  $element = &$variables['element'];
  $content = $element['#value']['TagBody'];
  unset($element['#value']);
  if (!empty($content)) {
    return '<script type="application/ld+json">' . $content . '</script>';
  };
}
