<?php
/**
 * @file
 * Features integration for the Jsontag module.
 */

/**
 * Implements hook_features_export().
 */
function jsontag_features_export($data, &$export, $module_name = '', $type = 'jsontag') {
  $pipe = array();

  foreach ($data as $name) {
    if (jsontag_config_load($name)) {
      $export['features'][$type][$name] = $name;
    }
  }

  $export['dependencies']['jsontag'] = 'jsontag';

  return $pipe;
}

/**
 * Implements hook_features_export_render().
 */
function jsontag_features_export_render($module_name, $data, $export = NULL) {
  $code = array();
  $code[] = '  $config = array();';
  $code[] = '';

  foreach ($data as $key => $name) {
    if (is_object($name)) {
      $name = $name->instance;
    }
    if ($config = jsontag_config_load($name)) {
      $export = new stdClass();
      $export->instance = $config->instance;
      $export->config = $config->config;
      $export = features_var_export($export, '  ');
      $key = features_var_export($name);
      $code[] = "  // Exported Jsontag config instance: {$name}.";
      $code[] = "  \$config[{$key}] = {$export};";
      $code[] = "";
    }
  }
  $code[] = '  return $config;';
  $code = implode("\n", $code);
  return array('jsontag_export_default' => $code);
}

/**
 * Implements hook_features_revert().
 */
function jsontag_features_revert($module) {
  if ($feature_conf = features_get_default('jsontag', $module)) {
    foreach (array_keys($feature_conf) as $config) {
      if ($conf = jsontag_config_load($config)) {
        db_delete('jsontag_config')->condition('instance', $config)->execute();
      }
      unset($feature_conf[$config]['cid']);
      $object = new stdClass();
      $object->cid = NULL;
      $object->instance = $config;
      $object->config = $feature_conf[$config]['config'];
      jsontag_config_save($object);
    }
  }
}

/**
 * Implements hook_features_export_options().
 */
function jsontag_features_export_options() {
  $instances = jsontag_config_instance_info();
  foreach ($instances as $key => $instance) {
    $options[$key] = $key;
  };
  return $options;
}

/**
 * Implements hook_features_rebuild().
 */
function jsontag_features_rebuild($module) {
  jsontag_features_revert($module);
}
