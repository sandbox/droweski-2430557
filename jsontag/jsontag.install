<?php

/**
 * @file
 * Install, update, and uninstall functions for the jsontag module.
 */

/**
 * Implements hook_schema().
 */
function jsontag_schema() {
  $schema['jsontag_config'] = array(
    'description' => 'Storage of json tag configuration and defaults.',
    'export' => array(
      'key' => 'instance',
      'key name' => 'Instance',
      'primary key' => 'cid',
      'identifier' => 'config',
      'default hook' => 'jsontag_config_default',
      'api' => array(
        'owner' => 'jsontag',
        'api' => 'jsontag',
        'minimum_version' => 1,
        'current_version' => 1,
      ),
      'cache defaults' => TRUE,
      'default cache bin' => 'cache_jsontag',
    ),
    'fields' => array(
      'cid' => array(
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'description' => 'The primary identifier for a jsontag configuration set.',
        'no export' => TRUE,
      ),
      'instance' => array(
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
        'description' => 'The machine-name of the configuration, typically entity-type:bundle.',
      ),
      'config' => array(
        'type' => 'blob',
        'size' => 'big',
        'not null' => TRUE,
        'serialize' => TRUE,
        'description' => 'Serialized data containing the json tag configuration.',
        'translatable' => TRUE,
      ),
    ),
    'primary key' => array('cid'),
    'unique keys' => array(
      'instance' => array('instance'),
    ),
  );

  $schema['jsontag'] = array(
    'fields' => array(
      'entity_type' => array(
        'type' => 'varchar',
        'length' => 32,
        'not null' => TRUE,
        'default' => '',
        'description' => 'The entity type this data is attached to',
      ),
      'entity_id' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
        'description' => 'The entity id this data is attached to',
      ),
      // @todo Enable revisionable json tags.
      'data' => array(
        'type' => 'blob',
        'size' => 'big',
        'not null' => TRUE,
        'serialize' => TRUE,
      ),
      'language' => array(
        'type' => 'varchar',
        'length' => 32,
        'not null' => TRUE,
        'default' => '',
        'description' => 'The language of the tag.',
      ),
    ),
    'primary key' => array('entity_type', 'entity_id', 'language'),
  );

  $schema['cache_jsontag'] = drupal_get_schema_unprocessed('system', 'cache');
  $schema['cache_jsontag']['description'] = t('Cache table for the generated json tag output.');

  return $schema;
}

/**
 * Implements hook_requirements().
 */
function jsontag_requirements($phase) {
  $requirements = array();
  // Ensure translations don't break during installation.
  $t = get_t();

  if ($phase == 'runtime') {
    // Work out the release of D7 that is currently running.
    list($major, $minor) = explode('.', VERSION);
    // Strip off any suffixes on the version string, e.g. "17-dev".
    if (strpos('-', $minor)) {
      list($minor, $suffix) = explode('-', $minor);
    }

    // Releases of Drupal older than 7.15 did not have entity_language(), which
    // is now required.
    if ($minor < 15) {
      $requirements['jsontag'] = array(
        'severity' => REQUIREMENT_WARNING,
        'title' => 'Jsontag',
        'value' => $t('Upgrade Drupal core to v7.15 or newer'),
        'description' => $t("This older version of Drupal core is missing functionality necessary for the module's multilingual support, it must be upgraded to at least version 7.15."),
      );
    }
    // Releases of Drupal older than 7.17 did not trigger hook_entity_view on
    // term pages, so recommend updating.
    elseif ($minor < 17) {
      $requirements['jsontag'] = array(
        'severity' => REQUIREMENT_WARNING,
        'title' => 'Jsontag',
        'value' => $t('Upgrade Drupal core to v7.17 or newer'),
        'description' => $t('Your older version of Drupal core is missing functionality necessary for taxonomy term pages to work correctly, it is strongly recommended to upgrade to the latest release.'),
      );
    }
    // Everything's OK.
    else {
      $requirements['jsontag'] = array(
        'severity' => REQUIREMENT_OK,
        'title' => 'Jsontag',
        'value' => $t('Drupal core is compatible'),
        'description' => $t('Older versions of Drupal core were missing functionality necessary for taxonomy term pages to work correctly, but this version <em>will</em> work correctly.'),
      );
    }

    // Add a note if Page Title is also installed.
    if (module_exists('page_title')) {
      $requirements['jsontag_page_title'] = array(
        'severity' => REQUIREMENT_INFO,
        'title' => 'Jsontag',
        'value' => $t('Possible conflicts with Page Title module'),
        'description' => $t('The Jsontag module is able to customize page titles so running the Page Title module simultaneously can lead to complications.'),
      );
    }

    // Add a note if Node Title is also installed.
    if (module_exists('exclude_node_title')) {
      $requirements['jsontag_exclude_node_title'] = array(
        'severity' => REQUIREMENT_INFO,
        'title' => 'Jsontag',
        'value' => $t('Possible conflicts with Exclude Node Title module'),
        'description' => $t('The Jsontag module\'s default settings for content types (nodes) uses [node:title] for the page title. Unfortunately, Exclude Node Title hides this so the page title ends up blank. It is recommended to <a href="!config">change the "title" field\'s default value</a> to "[current-page:title]" instead of "[node:title]" for any content types affected by Exclude Node Title.', array('!config' => 'admin/config/search/jsontags')),
      );
    }

    // Add a note if the deprecated jsontag.entity_translation.inc file still
    // exists.
    $filename = 'jsontag.entity_translation.inc';
    if (file_exists(dirname(__FILE__) . '/' . $filename)) {
      $requirements['jsontag_deprecated_et_file'] = array(
        'severity' => REQUIREMENT_ERROR,
        'title' => 'Jsontag',
        'value' => $t('Unwanted :filename file found', array(':filename' => $filename)),
        'description' => $t("The :filename file was removed in v7.x-1.0-beta5 but it still exists in the site's Jsontag module's directory and will cause problems. This file needs to be removed. The file's path in the Drupal directory structure is:<br /><code>!short_path</code><br />The file's full path is:<br /><code>!full_path</code>", array(':filename' => $filename, '!short_path' => drupal_get_path('module', 'jsontag') . '/' . $filename, '!full_path' => dirname(__FILE__) . $filename)),
      );
    }
  }

  return $requirements;
}

/**
 * Implements hook_uninstall().
 */
function jsontag_uninstall() {
  // This variable is created via hook_enable.
  variable_del('jsontag_schema_installed');
}

/**
 * Implements hook_enable().
 */
function jsontag_enable() {
  variable_set('jsontag_schema_installed', TRUE);
}
