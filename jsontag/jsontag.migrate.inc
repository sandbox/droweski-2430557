<?php

/**
 * @file
 * Jsontag support for Migrate.
 */

/**
 * Basic usage of the Migrate integration.
 *
 * This example assumes the custom module's name is "example_migrate".
 * 
 * example_migrate.inc:
 * 
 * class JsontagTestMigration extends DynamicMigration {
 * 
 *   public function __construct() {
 *     parent::__construct();
 * 
 *     $this->description = t('Migrate test.');
 * 
 *     $this->map = new MigrateSQLMap(
 *       $this->machineName,
 *       array(
 *         'id' => array(
 *           'type' => 'varchar',
 *           'not null' => TRUE,
 *           'length' => 254,
 *           'description' => 'ID of record.',
 *         ),
 *       ),
 *       MigrateDestinationNode::getKeySchema()
 *     );
 * 
 *     $this->source = new MigrateSourceCSV(
 *       drupal_get_path('module', 'example_migrate') . '/sample.csv',
 *       array(),
 *       array('header_rows' => TRUE)
 *     );
 * 
 *     $this->destination = new MigrateDestinationNode('article');
 * 
 *     $this->addFieldMapping('jsontag_description', 'description');
 *     $this->addFieldMapping('jsontag_keywords', 'keywords');
 *   }
 * }
 * 
 * example_migrate.migrate.inc:
 * 
 * /**
 *  * Implements hook_migrate_api().
 *  * /
 * function example_migrate_migrate_api() {
 *   $api = array(
 *     'api' => 2,
 *     'migrations' => array(
 *       'JsontagTest' => array('class_name' => 'JsontagTestMigration'),
 *     ),
 *   );
 * 
 *   return $api;
 * }
 */

/**
 * Implements hook_migrate_api().
 */
function jsontag_migrate_api() {
  $api = array(
    'api' => 2,
    'destination handlers' => array(
      'MigrateJsontagHandler',
    ),
  );

  return $api;
}

/**
 * Jsontag destination handler.
 */
class MigrateJsontagHandler extends MigrateDestinationHandler {

  public function __construct() {
    $types = array();
    foreach (entity_get_info() as $entity_type => $entity_info) {
      if (isset($entity_info['jsontags']) && $entity_info['jsontags']) {
        $types[] = $entity_type;
      }
    }

    $this->registerTypes($types);
  }

  /**
   * Implements MigrateDestinationHandler::fields().
   */
  public function fields() {
    $fields = array();
    $elements = jsontag_get_info();

    foreach ($elements['tags'] as $value) {
      $jsontag_field = 'jsontag_' . $value['name'];
      $fields[$jsontag_field] = $value['description'];
    }

    return $fields;
  }

  /**
   * Implements MigrateDestinationHandler::prepare().
   */
  public function prepare($entity, stdClass $row) {
    $elements = jsontag_get_info();

    foreach ($elements['tags'] as $value) {
      $jsontag_field = 'jsontag_' . $value['name'];
      if (isset($entity->$jsontag_field)) {
        $entity->jsontags[$value['name']]['value'] = $entity->$jsontag_field;
        unset($entity->$jsontag_field);
      }
    }
  }
}
