<?php

/**
 * @file
 * Class definitions for the jsontag module.
 */

interface DrupalJsonTagInterface {

  /**
   * Constructor
   *
   * @param array $info
   *   The information about the json tag from jsontag_get_info().
   */
  function __construct(array $info, array $data = array());

  function getForm();

  function getValue();

  function getElement();
}

class DrupalDefaultJsonTag implements DrupalJsonTagInterface {

  protected $info;
  protected $data = array('value' => '');

  function __construct(array $info, array $data = NULL) {
    $this->info = $info;
    if (isset($data)) {
      $this->data = $data;
    }
  }

  public function getForm(array $options = array()) {
    return array();
  }

  public function getValue(array $options = array()) {
    return $this->data['value'];
  }

  public function getElement(array $options = array()) {
    $element = isset($this->info['element']) ? $this->info['element'] : array();

    $value = $this->getValue($options);
    if (strlen($value) === 0) {
      return array();
    }

    $element += array(
      '#theme' => 'jsontag',
      '#tag' => 'json',
      '#id' => 'jsontag_' . $this->info['name'],
      '#name' => $this->info['name'],
      '#value' => $value,
    );

    // Add header information if desired.
    if (!empty($this->info['header'])) {
      $element['#attached']['drupal_add_http_header'][] = array($this->info['header'], $value);
    }

    return array(
      '#attached' => array('drupal_add_html_head' => array(array($element, $element['#id']))),
    );
  }
}

/**
 * Text-based json tag controller.
 */
class DrupalTextJsonTag extends DrupalDefaultJsonTag {

  public function getForm(array $options = array()) {
    $options += array(
      'token types' => array(),
    );

    $form['value'] = isset($this->info['form']) ? $this->info['form'] : array();

    $form['value'] += array(
      '#type' => 'textfield',
      '#title' => $this->info['label'],
      '#description' => !empty($this->info['description']) ? $this->info['description'] : '',
      '#default_value' => isset($this->data['value']) ? $this->data['value'] : '',
      '#element_validate' => array('token_element_validate'),
      '#token_types' => $options['token types'],
      '#maxlength' => 1024,
    );

    return $form;
  }

  public function getValue(array $options = array()) {
    $name = "jsontag:" . $options["instance"] . ":" . $this->info["name"];

    $options += array(
      'token data' => array(),
      'clear' => TRUE,
      'sanitize' => TRUE,
      'raw' => FALSE,
    );

    $value = jsontag_translate($name, $this->data['value']);
    if (empty($options['raw'])) {
      // Give other modules the opportunity to use hook_jsontag_pattern_alter()
      // to modify defined token patterns and values before replacement.
      drupal_alter('jsontag_pattern', $value, $options['token data']);
      $value = token_replace($value, $options['token data'], $options);
    }
    $value = strip_tags(decode_entities($value));
    $value = trim($value);
    return $value;
  }
}

/**
 * Text-area json tag controller.
 */
class DrupalTextAreaJsonTag extends DrupalDefaultJsonTag {

  public function getForm(array $options = array()) {
    $options += array(
        'token types' => array(),
    );

    $form['value'] = isset($this->info['form']) ? $this->info['form'] : array();

    $form['value'] += array(
        '#type' => 'textarea',
        '#title' => $this->info['label'],
        '#description' => !empty($this->info['description']) ? $this->info['description'] : '',
        '#default_value' => isset($this->data['value']) ? $this->data['value'] : '',
        '#element_validate' => array('token_element_validate'),
        '#token_types' => $options['token types'],
        '#maxlength' => 2048,
    );

    return $form;
  }

  public function getValue(array $options = array()) {
    $name = "jsontag:" . $options["instance"] . ":" . $this->info["name"];

    $options += array(
        'token data' => array(),
        'clear' => TRUE,
        'sanitize' => TRUE,
        'raw' => FALSE,
    );

    $value = jsontag_translate($name, $this->data['value']);
    $json = json_decode(strip_tags($value), TRUE);
    if (empty($options['raw'])) {
      // Give other modules the opportunity to use hook_jsontag_pattern_alter()
      // to modify defined token patterns and values before replacement.
      drupal_alter('jsontag_pattern', $value, $options['token data']);

      foreach ($json as $key => $val) {
        $json[$key] = token_replace($val, $options['token data'], $options);
      }
      $value = json_encode($json, JSON_PRETTY_PRINT);
    }
    $value = strip_tags(decode_entities($value));
    $value = trim($value);
    return $value;
  }
}

/**
 * Link type json tag controller.
 */
class DrupalLinkJsonTag extends DrupalTextJsonTag {

  public function getElement(array $options = array()) {
    $element = isset($this->info['element']) ? $this->info['element'] : array();

    $value = $this->getValue($options);
    if (strlen($value) === 0) {
      return array();
    }

    $element += array(
      '#theme' => 'jsontag_link_rel',
      '#tag' => 'link',
      '#id' => 'jsontag_' . $this->info['name'],
      '#name' => $this->info['name'],
      '#value' => $value,
    );

    if (!isset($this->info['header']) || !empty($this->info['header'])) {
      // Also send the generator in the HTTP header.
      // @todo This does not support 'rev' or alternate link headers.
      $element['#attached']['drupal_add_http_header'][] = array('Link', '<' . check_plain($value) . '>;' . drupal_http_header_attributes(array('rel' => $element['#name'])), TRUE);
    }

    return array(
      '#attached' => array('drupal_add_html_head' => array(array($element, $element['#id']))),
    );
  }
}

/**
 * Title json tag controller.
 *
 * This extends DrupalTextJsonTag as we need to alter variables in
 * template_preprocess_html() rather output a normal json tag.
 */
class DrupalTitleJsonTag extends DrupalTextJsonTag {

  public function getElement(array $options = array()) {
    $element = array();
    $value = check_plain($this->getValue($options));
    $element['#attached']['jsontag_set_preprocess_variable'][] = array('html', 'head_title', $value);
    $element['#attached']['jsontag_set_preprocess_variable'][] = array('html', 'head_array', array('title' => $value));
    return $element;
  }
}

/**
 * Multiple value json tag controller.
 */
class DrupalListJsonTag extends DrupalDefaultJsonTag {

  public function getForm(array $options = array()) {
    $form['value'] = isset($this->info['form']) ? $this->info['form'] : array();

    $form['value'] += array(
      '#type' => 'checkboxes',
      '#title' => $this->info['label'],
      '#description' => !empty($this->info['description']) ? $this->info['description'] : '',
      '#default_value' => isset($this->data['value']) ? $this->data['value'] : array(),
    );

    return $form;
  }

  public function getValue(array $options = array()) {
    $values = array_keys(array_filter($this->data['value']));
    sort($values);
    return implode(', ', $values);
  }
}
