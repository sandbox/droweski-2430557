Jsontag
-------
The JT module is designed to improve Search Engine Results Page performance.
It inserts a JSON-LD script into the HTML header that contains structured data
about the page content.

Module Configuration
------------------------------------------------------------------------------
 1. On the People Permissions administration page ("Administer >> People
    >> Permissions") you need to assign:

    - The "Administer json tags" permission to the roles that are allowed to
      access the json tags admin pages to control the site defaults.

    - The "Edit json tags" permission to the roles that are allowed to change
      json tags on each individual page (node, term, etc).


Tag Configuration
------------------------------------------------------------------------------
 Setup Steps
 Generate the JSON-LD template for your content type using the Google Markup Helper
 online app.
 Paste this template into the JT field and replace the variables with Drupal tokens.
 Confirm that the page source code includes the appropriate script values.

 1. Generate Template
 Generate the JSON-LD structured data template using the Google Markup Helper online app.
 Pick a typical page of the content type you want to mark up.
 Go to: https://www.google.com/webmasters/markup-helper/
 Choose the data type that matches your content.
 Paste in your page's url or HTML.
 Click on the “Start Tagging” button.
 After a bit of processing, you will be redirected to the content tagging page.

 On the tagging page, you will see a list of available tags and your page content.
 Select the appropriate text area in your content and apply the tag from the dropdown list.
 After tagging is complete, click the “Create HTML” button.
 On the markup screen, change the type to JSON-LD
 Copy the generated JSON script.

 2. JT Module Setup
 Navigate to module admin page: admin/config/search/jsontags
 Module administration is similar to metatags module.
 The main administrative page controls the site-wide defaults, both global
 settings and defaults per entity (node, term, etc), in addition to those
 assigned specifically for the front page.
 Add a default for the desired content type.
 Paste your generated script into the module markup field.
 Replace constant values with tokens.

 3. Confirm Script
 Navigate to an appropriate page.
 Display the page source code.
 You should see the JSON-LD script in the HTML header.
 Verify that the template and token replacements are displaying the correct information
 for the page.
